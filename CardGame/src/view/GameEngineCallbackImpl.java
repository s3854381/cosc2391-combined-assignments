package view;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.interfaces.GameEngine;
import model.interfaces.Player;
import model.interfaces.PlayingCard;
import view.interfaces.GameEngineCallback;

/**
 * Implementation of GameEngineCallback with Java logging behaviour.
 * 
 * @author Caspar Ryan - skeleton, partial example
 * @author Michael Asquith - finished implementation
 * @see view.interfaces.GameEngineCallback
 * 
 */
public class GameEngineCallbackImpl implements GameEngineCallback
{
	public static final Logger logger = Logger.getLogger(GameEngineCallbackImpl.class.getName());

	// Logging levels. Intermediate results at FINE, final results at INFO.
	private Level bustLevel = Level.INFO;
	private Level cardLevel = Level.INFO;
	private Level resultLevel = Level.INFO;

	/** utility method to set output level of logging handlers */
	public static Logger setAllHandlers(Level level, Logger logger, boolean recursive)
	{
		// end recursion?
		if (logger != null)
		{
			logger.setLevel(level);
			for (Handler handler : logger.getHandlers())
				handler.setLevel(level);
			// recursion
			setAllHandlers(level, logger.getParent(), recursive);
		}
		return logger;
	}

	/**
	 * Creates a GameEngineCallbackImpl.
	 */
	public GameEngineCallbackImpl()
	{
		// NOTE can also set the console to FINE in %JRE_HOME%\lib\logging.properties
		setAllHandlers(Level.INFO, logger, true);
	}

	/**
	 * called for each card as the house is dealing to a Player
	 * 
	 * @param player
	 *            the Player who is receiving cards
	 * @param card
	 *            the next card that was dealt
	 * @param engine
	 *            a convenience reference to the engine so the receiver can call
	 *            methods if necessary
	 * @see model.interfaces.GameEngine
	 */	
	@Override
	public void nextCard(Player player, PlayingCard card, GameEngine engine)
	{
		logger.log(cardLevel, formatNextCard(player.getPlayerName(), card));
	}

	/**
	 * called when the card causes the player to bust <br>
	 * this method is called instead of {@link #nextCard(Player, PlayingCard, GameEngine)} <br>
	 * this method is called before {@link #result(Player, int, GameEngine)} <br><br>
	 * 
	 * NOTE: If player gets 42 exactly then this method IS NOT called
	 *     
	 * @param player - the Player who is receiving cards
	 * @param card - the bust card that was dealt
	 * @param engine - a convenience reference to the engine so the receiver can call
	 * methods if necessary
	 * @see model.interfaces.GameEngine
	 */
	@Override
	public void bustCard(Player player, PlayingCard card, GameEngine engine)
	{
		logger.log(bustLevel, formatBustCard(player.getPlayerName(), "YOU", card));
	}

	/**
	 * called after the player has bust with final result (result is score prior
	 * to the last card that caused the bust)
	 * 
	 * Called from {@link GameEngine#dealPlayer(Player, int)} 
	 * 
	 * @param player - the current Player
	 * @param result - the final score of the hand
	 * @param engine - a convenience reference to the engine so the receiver can call
	 * methods if necessary
	 * @see model.interfaces.GameEngine
	 */
	@Override
	public void result(Player player, int result, GameEngine engine)
	{
		logger.log(resultLevel, formatResult(player.getPlayerName(), result));
	}

	/**
	 * called as the house is dealing their own hand
	 *     
	 * @param card - the next card that was dealt
	 * @param engine - a convenience reference to the engine so the receiver can call
	 * methods if necessary
	 * @see model.interfaces.GameEngine
	 */
	@Override
	public void nextHouseCard(PlayingCard card, GameEngine engine)
	{
		logger.log(cardLevel, formatNextCard("House", card));

	}
	/**
	 * HOUSE version of {@link GameEngineCallback#bustCard(Player, PlayingCard, GameEngine)}
	 * <br><br>
	 * NOTE: If house gets 42 exactly then this method IS NOT called
	 *   
	 * @param card - the bust card that was dealt
	 * @param engine - a convenience reference to the engine so the receiver can call
	 * methods if necessary
	 * @see model.interfaces.GameEngine
	 */
	@Override
	public void houseBustCard(PlayingCard card, GameEngine engine)
	{
		logger.log(bustLevel, formatBustCard("House", "HOUSE", card));
	}

	/**
	 * called when the HOUSE has bust with final result (result is score prior to the last card
	 * that caused the bust)
	 *     
	 * PRE-CONDITION: This method should only be called AFTER bets have been updated on all Players 
	 * so this callback can log Player results
	 * 
	 * After displaying the house result, it also displays the results of all players.
	 * 
	 * Called from {@link GameEngine#dealHouse(int)} 
	 * 
	 * @param result - the final score of the dealers (house) hand
	 * @param engine - a convenience reference to the engine so the receiver can call
	 * methods if necessary
	 * @see model.interfaces.GameEngine
	 */
	@Override
	public void houseResult(int result, GameEngine engine)
	{
		// Display house result
		logger.log(resultLevel, formatResult("House", result));

		// Collect player results
		String playerResults = "Final Player Results";
		for (Player player : engine.getAllPlayers())
		{
			playerResults += String.format("\n%s", player);
		}

		// Display player results
		logger.log(resultLevel, playerResults);
	}

	/**
	 * Next Card string formatting for player and house.
	 * 
	 * @param name - name of the player or the house
	 * @param card - card dealt to them
	 * @return formatted string for display
	 */
	private String formatNextCard(String name, PlayingCard card)
	{
		return String.format("Card Dealt to %s .. %s", name, card);
	}

	/**
	 * Bust card string formatting for player and house.
	 * 
	 * @param name - name of the player or the house
	 * @param bustName - name to display in the final line "_____ BUSTED!"
	 * @param card - card that busted the player
	 * @return formatted string for display
	 */
	private String formatBustCard(String name, String bustName, PlayingCard card)
	{
		return String.format("%s .. %s BUSTED!", formatNextCard(name, card), bustName);
	}

	/**
	 * Result string formatted for player and house.
	 * 
	 * @param name - name of the player or the house
	 * @param result - total score for the player or the house
	 * @return formatted string for display
	 */
	private String formatResult(String name, int result)
	{
		return String.format("%s, final result=%d", name, result);
	}
}
