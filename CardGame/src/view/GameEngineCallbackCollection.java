package view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import model.interfaces.GameEngine;
import model.interfaces.Player;
import model.interfaces.PlayingCard;
import view.interfaces.GameEngineCallback;

/**
 * A collection of {@link GameEngineCallback} that can also be treated as a GameEngineCallback
 * itself. When a GameEngineCallback method is called, that method is called on each
 * GameEngineCallback in the collection in the same order they were inserted in. A
 * GameEngineCallbackCollection does not implement GameEngineCallback on its own, relying on
 * its members implementations instead. The Collection interface is implemented through an
 * {@link ArrayList} delegate.
 * 
 * @author Michael Asquith
 */
public class GameEngineCallbackCollection<E extends GameEngineCallback>
		implements GameEngineCallback, Collection<GameEngineCallback>
{
	private Collection<GameEngineCallback> delegate;

	/**
	 * Constructs a new empty GameEngineCallbackCollection.
	 */
	public GameEngineCallbackCollection() { delegate = new ArrayList<>(); }

	/**
	 * Constructs a GameEngineCallbackCollection, with copies of all elements in collection.
	 * 
	 * @param collection - a collection of GameEngineCallbacks to copy.
	 */
	public GameEngineCallbackCollection(Collection<GameEngineCallback> collection) {
		delegate = new ArrayList<>(collection);
	}

	/**
	 * Called for each card as the house is dealing to a Player
	 * 
	 * @param player the Player who is receiving cards
	 * @param card   the next card that was dealt
	 * @param engine a convenience reference to the engine so the receiver can call methods if
	 *               necessary
	 * @see model.interfaces.GameEngine
	 */
	@Override
	public void nextCard(Player player, PlayingCard card, GameEngine engine) {
		for (GameEngineCallback gec : this) gec.nextCard(player, card, engine);
	}

	/**
	 * Called when the card causes the player to bust <br>
	 * this method is called instead of {@link #nextCard(Player, PlayingCard, GameEngine)}
	 * <br>
	 * this method is called before {@link #result(Player, int, GameEngine)} <br>
	 * <br>
	 * NOTE: If player gets 42 exactly then this method IS NOT called
	 * 
	 * @param player - the Player who is receiving cards
	 * @param card   - the bust card that was dealt
	 * @param engine - a convenience reference to the engine so the receiver can call methods
	 *               if necessary
	 * @see model.interfaces.GameEngine
	 */
	@Override
	public void bustCard(Player player, PlayingCard card, GameEngine engine) {
		for (GameEngineCallback gec : this) gec.bustCard(player, card, engine);
	}

	/**
	 * Called after the player has bust with final result (result is score prior to the last
	 * card that caused the bust) Called from {@link GameEngine#dealPlayer(Player, int)}
	 * 
	 * @param player - the current Player
	 * @param result - the final score of the hand
	 * @param engine - a convenience reference to the engine so the receiver can call methods
	 *               if necessary
	 * @see model.interfaces.GameEngine
	 */
	@Override
	public void result(Player player, int result, GameEngine engine) {
		for (GameEngineCallback gec : this) gec.result(player, result, engine);
	}

	/**
	 * Called as the house is dealing their own hand
	 * 
	 * @param card   - the next card that was dealt
	 * @param engine - a convenience reference to the engine so the receiver can call methods
	 *               if necessary
	 * @see model.interfaces.GameEngine
	 */
	@Override
	public void nextHouseCard(PlayingCard card, GameEngine engine) {
		for (GameEngineCallback gec : this) gec.nextHouseCard(card, engine);
	}

	/**
	 * HOUSE version of {@link GameEngineCallback#bustCard(Player, PlayingCard, GameEngine)}
	 * <br>
	 * <br>
	 * NOTE: If house gets 42 exactly then this method IS NOT called
	 * 
	 * @param card   - the bust card that was dealt
	 * @param engine - a convenience reference to the engine so the receiver can call methods
	 *               if necessary
	 * @see model.interfaces.GameEngine
	 */
	@Override
	public void houseBustCard(PlayingCard card, GameEngine engine) {
		for (GameEngineCallback gec : this) gec.houseBustCard(card, engine);
	}

	/**
	 * Called when the HOUSE has bust with final result (result is score prior to the last
	 * card that caused the bust) PRE-CONDITION: This method should only be called AFTER bets
	 * have been updated on all Players so this callback can log Player results Called from
	 * {@link GameEngine#dealHouse(int)}
	 * 
	 * @param result - the final score of the dealers (house) hand
	 * @param engine - a convenience reference to the engine so the receiver can call methods
	 *               if necessary
	 * @see model.interfaces.GameEngine
	 */
	@Override
	public void houseResult(int result, GameEngine engine) {
		for (GameEngineCallback gec : this) gec.houseResult(result, engine);
	}

	/**
	 * Appends the specified element to the end of this list. Cannot add a null
	 * GameEngineCallback.
	 * 
	 * @param gec - GameEngineCallback to be appended to this list.
	 * @return true (as specified by {@link java.util.Collection#add})
	 */
	@Override
	public boolean add(GameEngineCallback gec) {
		boolean result = false;
		if (gec != null) result = delegate.add(gec);
		return result;
	}

	/**
	 * If all elements in the collection are non-null, they are added. Otherwise none are
	 * added.
	 * 
	 * @param gec - GameEngineCallback to be appended to this list.
	 * @return true (as specified by {@link java.util.Collection#add})
	 */
	@Override
	public boolean addAll(Collection<? extends GameEngineCallback> arg0) {
		boolean result = false;
		if (!arg0.contains(null)) result = delegate.addAll(arg0);
		return result;
	}

	/**
	 * @see {@link java.util.Collection#clear()}
	 */
	public void clear() { delegate.clear(); }

	/**
	 * @param o
	 * @return
	 * @see {@link java.util.Collection#contains(java.lang.Object)}
	 */
	public boolean contains(Object o) { return delegate.contains(o); }

	/**
	 * @param c
	 * @return
	 * @see {@link java.util.Collection#containsAll(java.util.Collection)}
	 */
	public boolean containsAll(Collection<?> c) { return delegate.containsAll(c); }

	/**
	 * @return
	 * @see {@link java.util.Collection#isEmpty()}
	 */
	public boolean isEmpty() { return delegate.isEmpty(); }

	/**
	 * @return
	 * @see {@link java.util.Collection#iterator()}
	 */
	public Iterator<GameEngineCallback> iterator() { return delegate.iterator(); }

	/**
	 * @param o
	 * @return
	 * @see {@link java.util.Collection#remove(java.lang.Object)}
	 */
	public boolean remove(Object o) { return delegate.remove(o); }

	/**
	 * @param c
	 * @return
	 * @see {@link java.util.Collection#removeAll(java.util.Collection)}
	 */
	public boolean removeAll(Collection<?> c) { return delegate.removeAll(c); }

	/**
	 * @param c
	 * @return
	 * @see {@link java.util.Collection#retainAll(java.util.Collection)}
	 */
	public boolean retainAll(Collection<?> c) { return delegate.retainAll(c); }

	/**
	 * @return
	 * @see {@link java.util.Collection#size()}
	 */
	public int size() { return delegate.size(); }

	/**
	 * @return
	 * @see {@link java.util.Collection#toArray()}
	 */
	public Object[] toArray() { return delegate.toArray(); }

	/**
	 * @param <T>
	 * @param a
	 * @return
	 * @see {@link java.util.Collection#toArray(java.lang.Object[])}
	 */
	public <T> T[] toArray(T[] a) { return delegate.toArray(a); }
}
