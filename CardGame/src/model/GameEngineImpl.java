package model;

import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

import model.interfaces.GameEngine;
import model.interfaces.Player;
import model.interfaces.PlayingCard;
import model.interfaces.PlayingCard.Suit;
import model.interfaces.PlayingCard.Value;
import view.GameEngineCallbackCollection;
import view.interfaces.GameEngineCallback;

/**
 * GameEngine for implementing the game described in the Further Programming Semester 2 2020
 * Assignment 1 specification.<br><br>
 * 
 * Holds collections of {@link Player} and {@link GameEngineCallback} objects, which need to
 * be added to with {@link GameEngineImpl#addPlayer} and
 * {@link GameEngineImpl#addGameEngineCallback} before the game can be run and displayed.
 * <br><br>
 * 
 * Once Players and GameEngineCallbacks have been added, each player will need a bet and a
 * need to be dealt cards with {@link GameEngineImpl#placeBet} and
 * {@link GameEngineImpl#dealPlayer} respectively. The order of these two operations
 * is up to the caller of the GameEngineImpl.<br><br>
 * 
 * Finally, {@link GameEngineImpl#dealHouse} will deal for the house, settle the results
 * of all players, and reset their bets.<br><br>
 * 
 * The game is played as follows:<br><br>
 * 
 * The deck contains the 28 possible {@link PlayingCard}. Each is worth their Blackjack
 * value, except for the aces, which are worth exactly 11. A player is dealt cards until
 * they bust or reach the maximum score of 42. If a player busts, their result is their
 * score before being dealt the busting card. If their score is 42, that is their result.
 * <br><br>
 * 
 * On a higher score than the dealer, the player gains their bet in betting points. On a
 * lower score than the dealer, they lose that many points. If they are tied with the dealer
 * their betting points do not change.
 * 
 * @author Michael Asquith
 */
public class GameEngineImpl implements GameEngine
{

	private GameEngineCallbackCollection<GameEngineCallback> allCallbacks;
	private Map<String, Player> allPlayers;
	private Queue<PlayingCard> deck;
	
	/**
	 * The {@link NullSimplePlayer} breaks some of the contracts of the Player interface. Most
	 * importantly {@link NullSimplePlayer#compareTo()} always throws an exception. See the
	 * NullSimplePlayer documentation for more details. <br><br>
	 * 
	 * DESIGN NOTE: Instantiated as NullSimplePlayer to make more explicit that this breaks some
	 * of the {@link Player} interface contracts, even though using Player here would be more
	 * Object-Oriented appropriate.
	 */
	private NullSimplePlayer house;

	/**
	 * Creates a new GameEngineImpl. The GameEngineImpl has a freshly shuffled "HALF" deck of
	 *  {@link PlayingCard} objects, and no {@link Player} or {@link GameEngineCallback} objects.
	 */
	public GameEngineImpl()
	{
		allCallbacks = new GameEngineCallbackCollection<>();
		// TreeMap is sorted by order of keys.
		allPlayers = new TreeMap<>();
		deck = getShuffledHalfDeck();
		// NullSimplePlayer breaks some of the Player interface 
		house = new NullSimplePlayer("The House");
	}

	/**
	 * Deals cards to a player until they reach the bust level, with the specified delay after each
	 * card is dealt.
	 * 
	 * @param player - the current player who will have their result set at the end of the hand
	 * @param delay - the delay between cards being dealt (in milliseconds (ms))
	 * @throws IllegalArgumentException thrown when delay param is {@literal <} 0 or {@literal >} 1000
	 * @throws IllegalArgumentException thrown if the player has not been previously added to the
	 * GameEngine
	 */
	@Override
	public void dealPlayer(Player player, int delay) throws IllegalArgumentException
	{
		// Preconditions
		if (delay > 1000) throw new IllegalArgumentException();
		if (!allPlayers.containsValue(player)) throw new IllegalArgumentException(
				String.format("Player %s not added to GameEngine", player));

		// Only deal for the player if they have bet.
		if (player.getBet() > 0) {
			int result = dealHand(player, delay);
			allCallbacks.result(player, result, this);

			// Update the player with the final result for later use
			player.setResult(result);
		}
	}

	/**
	 * Deals cards to the house until they reach the bust level, with the specified delay after each
	 * card is dealt. Once the house has been dealt to, the results of all players is announced and
	 * their bets are reset.
	 * 
	 * @param delay - the delay between cards being dealt (in milliseconds (ms))
	 * @throws IllegalArgumentException thrown when delay param is {@literal <} 0
	 * @see GameEngine#dealPlayer(Player, int)
	 */
	@Override
	public void dealHouse(int delay) throws IllegalArgumentException
	{
		int houseResult = dealHand(house, delay);

		// Update each player's points
		for(Player player : allPlayers.values()) {
			applyWinLoss(player, houseResult);
		}

		allCallbacks.houseResult(houseResult, this);

		// Reset bets for the next round.
		for(Player player : allPlayers.values()) {
			player.resetBet();
		}
	}

	/**
	 * A player's bet is settled by this method i.e. win or loss is applied to update betting
	 * points based on a comparison of the player result and the provided houseResult.
	 * 
	 * NOTE: This method is usually called from dealHouse(int) as described above but is
	 * included in the public interface to facilitate testing.
	 * 
	 * @param player - the Player to apply win/loss to
	 * @param houseResult - contains the calculated house result
	 */
	@Override
	public void applyWinLoss(Player player, int houseResult)
	{
		/** ASSUMPTION: This is public for the sake of testing this assignment, and
		 * in the field this would be a private method. As such we do not need to
		 * check if player is in the local collection, because this is only called
		 * while iterating over that collection */

		int points = player.getPoints();
		int bet = player.getBet();
		int playerResult = player.getResult();

		// Win
		if (playerResult > houseResult)
		{
			points += bet;
		} 
		// Draw
		else if (playerResult == houseResult)
		{
			; // No change in player's points
		}
		// Loss
		else if (playerResult < houseResult)
		{
			points -= bet;
		}

		player.setPoints(points);
	}

	/**
	 * Add a player to the game. If there is already a player with the same id, that player will be
	 * replaced by this new one. Null Players are discarded, and a player with a null ID will throw
	 * an IllegalArgumentException.
	 *  
	 * @param player - to add to game
	 * @throws IllegalArgumentException thrown if player.getPlayerId() == null.
	 */
	@Override
	public void addPlayer(Player player)
	{
		// Precondition
		if (player.getPlayerId() == null) throw new IllegalArgumentException();

		if (player != null) {
			// Map.put(key, value) has a set of keys, and will replace the old player with the new one.
			allPlayers.put(player.getPlayerId(), player);
		}
	}

	/**
	 * Returns the Player from the game with the specified ID.
	 * 
	 * @param id - id of player to retrieve (null if not found)
	 * @return the Player or null if Player has not been added
	 */
	@Override
	public Player getPlayer(String id)
	{
		return allPlayers.get(id);
	}

	/**
	 * Removes a player from the game.
	 * 
	 * @param player - to remove from game
	 * @return true if the player was in the game and was removed
	 */
	@Override
	public boolean removePlayer(Player player)
	{
		boolean result = false;
		if (player != null) {
			result = allPlayers.remove(player.getPlayerId(), player);
		}
		return result;
	}

	/**
	 * Sets the bet for the player. The bet must be greater than zero, and the player must have
	 * sufficient points to place the bet.
	 * 
	 * @param player - the player who is placing the bet
	 * @param bet - the bet in points
	 * @return true if the player had sufficient points and the bet was valid and placed
	 * @throws IllegalArgumentException thrown if the player has not been previously added
	 */
	@Override
	public boolean placeBet(Player player, int bet)
	{
		// Precondition
		if (!allPlayers.containsValue(player)) throw new IllegalArgumentException();

		return player.setBet(bet); 
	}

	/**
	 * Adds a GameEngineCallback to this GameEngine. The GameEngine-GameEngineCallback relationship is
	 * the connection between the model and the view. The GameEngineCallbacks are always called together,
	 * and they are called in the order they were added.
	 * 
	 * Null GameEngineCallbacks are not added as per GameEngineCallback.add().
	 * 
	 * @param gameEngineCallback -  a client specific implementation of GameEngineCallback used to perform
	 * display updates etc.
	 * @see view.interfaces.GameEngineCallback
	 */
	@Override
	public void addGameEngineCallback(GameEngineCallback gameEngineCallback)
	{
		allCallbacks.add(gameEngineCallback);
	}

	/**
	 * Removes a GameEngineCallback from this GameEngine.
	 * 
	 * @param gameEngineCallback - instance to be removed if no longer needed
	 * @return true if the gameEngineCallback existed and was removed
	 * @see view.interfaces.GameEngineCallback
	 */
	@Override
	public boolean removeGameEngineCallback(GameEngineCallback gameEngineCallback)
	{
		return allCallbacks.remove(gameEngineCallback);
	}

	/**
	 * Returns an unmodifiable collection of all Players, sorted in ascending order by player ID.
	 * 
	 * @return an unmodifiable collection (or a shallow copy) of all Players Collection is SORTED in
	 * ascending order by player id
	 * @see model.interfaces.Player
	 */
	@Override
	public Collection<Player> getAllPlayers()
	{
		return Collections.unmodifiableCollection(allPlayers.values());
	}

	/**
	 * Returns a shuffled "HALF" deck of cards containing 28 unique cards (8 through to Ace).
	 * 
	 * @return a Deque (specific type of Collection) of PlayingCard
	 * @see model.interfaces.PlayingCard
	 */
	@Override
	public Deque<PlayingCard> getShuffledHalfDeck()
	{
		// Shuffling requires a List, method returns a Deque. LinkedList fulfils both requirements.
		LinkedList<PlayingCard> deck = new LinkedList<>();

		for(Suit s : PlayingCard.Suit.values()) {
			for(Value v : PlayingCard.Value.values()) {
				deck.add(new PlayingCardImpl(s, v));
			}
		}
		Collections.shuffle(deck);
		return deck;
	}

	/**
	 * Plays a round of the game for the specified player.
	 * 
	 * @param player - player being dealt for. There is a house-specific player for dealing
	 * to the house.
	 * @param delay - delay in ms after each card is dealt
	 * @return the player's final score
	 * @throws IllegalArgumentException thrown if delay < 0
	 */
	private int dealHand(Player player, int delay) throws IllegalArgumentException
	{
		PlayingCard card;
		int result = 0;
		boolean isHouse = player.equals(house);

		do // while player.hand.result() < BUST_LEVEL // Default value 42
		{
			// Draw card
			if (deck.size() == 0) deck = getShuffledHalfDeck();
			card = deck.poll();

			// Deal a card to the player
			result += card.getScore();

			// Non-busting card
			if (result <= GameEngine.BUST_LEVEL) {

				if (isHouse) {
					allCallbacks.nextHouseCard(card, this);
				}
				else {
					allCallbacks.nextCard(player, card, this);
				}
			}
			// Busting card
			else { 
				if (isHouse) {
					allCallbacks.houseBustCard(card, this);
				}
				else {
					allCallbacks.bustCard(player, card, this);
				}
			}

			// Always delays after showing the card.
			try {
				Thread.sleep(delay);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}

		} while (result < GameEngine.BUST_LEVEL);

		// If the player busted, remove the last card dealt from the result
		if (result > GameEngine.BUST_LEVEL) result -= card.getScore();

		return result;
	}
}