/**
 * 
 */
package model;

import java.util.HashMap;
import java.util.Map;

import model.interfaces.PlayingCard;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Simple implementation of a playing card. Stores a suit and value. Can convert
 * the value to an integer value
 * 
 * @author Michael Asquith
 */
public class PlayingCardImpl implements PlayingCard
{	
	// Card scores in the same order as their respective Values. See getScore().
	private static final int[] CARD_SCORES = {11, 10, 10, 10, 10, 9, 8};
	private static Map<Value, Integer> scoreMap = null;
	
	private final Value value;
	private final Suit suit;
	
	/**
	 * Playing card that stores a suit and a value from the {@link PlayingCard} enumerations.
	 * 
	 * @param suit - suit of the card
	 * @param value - value of the card
	 */
	public PlayingCardImpl(Suit suit, Value value)
	{
		this.suit = suit;
		this.value = value;
	}

	/**
	 * Gets the suit of this card.
	 * 
	 * @return the suit of this card
	 */
	@Override
	public Suit getSuit()
	{
		return suit;
	}

	/**
	 * Gets the face value of this card.
	 * 
	 * @return the face value of this card
	 */
	@Override
	public Value getValue()
	{
		return value;
	}

	/**
	 * Returns the score of the card based on its {@link PlayingCard.Value}, which for this
	 * implementation is as follows:
	 * <pre>
	 * ACE  	11
	 * KING 	10
	 * QUEEN	10
	 * JACK 	10
	 * TEN  	10
	 * NINE 	9
	 * EIGHT	8
	 * </pre>
	 * 
	 * If new cards are added to the Value enumeration, this method must be altered or it will
	 * throw a NotImplementedException().
	 * 
	 * @return the score of this card
	 */
	@Override
	public int getScore()
	{
		if (scoreMap == null) {
			scoreMap = new HashMap<>();
			
			for (int i = 0; i < CARD_SCORES.length; ++i) {
				scoreMap.put(Value.values()[i], CARD_SCORES[i]);
			}
		}
		
		Integer returnValue = scoreMap.get(value);
		
		if (returnValue == null) {
			// Thrown only if new values are added to the Value Enum
			throw new NotImplementedException();
		}
		
		return returnValue;
	}

	/**
	 * Returns a string describing this card.
	 * 
	 * @return a human readable String that lists the values of this PlayingCard         
	 *         e.g. "Suit: Clubs, Value: Five, Score: 5" for Five of Clubs
	 */
	@Override
	public String toString()
	{
		String string = String.format("Suit: %s, Value: %s, Score: %d",
				toCapitalisedString(suit), toCapitalisedString(value), getScore());

		return string;
	}

	/**
	 * A card equals another card if both their suit and value are the same.
	 * 
	 * @param card - another card to compare with
	 * @return true - if the face value and suit is equal. Returns false if card is null
	 */
	@Override
	public boolean equals(PlayingCard card)
	{
		boolean result = false;
		if (card != null) {
			result = (suit == card.getSuit() && value == card.getValue());
		}
		return result;
	}

	/** 
	 * Casts and calls through to the type checked {@link PlayingCardImpl#equals(PlayingCard)}
	 * 
	 * @param card - another card to compare with
	 * @return true - if the face value and suit is equal. Returns false if compared with something
	 * that is not a PlayingCard.
	 */
	@Override
	public boolean equals(Object card)
	{
		boolean result = false;
		
		try {
			result = equals((PlayingCard) card);
		} catch(ClassCastException e) {}
		
		return result;
	}

	/**
	 * Based on Eclipse's auto-generated hashCodes.
	 * 
	 * @return hashCode based on the card's suit and value
	 */
	@Override
	public int hashCode() {
		int prime = 37;
		int result = 1;
		result = prime * result + ((suit != null) ? suit.hashCode() : 0);
		result = prime * result + ((suit != null) ? value.hashCode() : 0);
		return result;
	}
	
	/**
	 * Returns the string of an object, with the first character as {@link String#toUpperCase()},
	 * and the rest as {@link String#toLowerCase()}.
	 * 
	 * Could be useful in a utility class in the future.
	 * 
	 * @param object - object to return the string of
	 * @return String that starts with an upper case character
	 */
	private String toCapitalisedString (Object object)
	{
		String upperCase = object.toString().substring(0,1).toUpperCase();
		String lowerCase = object.toString().substring(1).toLowerCase();
		return upperCase + lowerCase;
	}
}
