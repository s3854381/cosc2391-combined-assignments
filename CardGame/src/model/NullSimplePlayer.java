/**
 * 
 */
package model;

import model.interfaces.Player;

/**
 * Special case of SimplePlayer with a null ID. setPoints(int), setBet(int) and compareTo(Player) are
 * defunct in this implementation, and will either throw an exception or do nothing. <br><br>
 * 
 * DESIGN NOTE: To allow the GameEngineImpl to reuse its dealHand(Player player, int delay) code, for
 * the house and players, I needed the house to be represented by its own Player. I also noticed that
 * because of the {@link Comparable#compareTo} method requirements in Player, that a null ID would
 * not be valid for a player which needs to be sorted.<br><br>
 * 
 * However, if there was a player which would never be sorted, that player could have a null ID.
 * Instead of disallowing the null ID in SimplePlayer completely, I decided to reserve it for this
 * class, to be used by the house for its private Player. Using up this corner of design space means
 * there cannot be a conflict between the player IDs of the house's private player, and the players
 * in its collection.
 * 
 * Technically this class does break the contract of the Player interface, but I have been very clear
 * how and where it does so.
 * 
 * @author Michael Asquith
 */
public class NullSimplePlayer extends SimplePlayer
{

	/**
	 * Creates a SimplePlayer with the given name, an id of null, and 0 betting points.
	 * 
	 * A NullSimplePlayer cannot have its points or bet set, and the compareTo() operator will
	 * always throw an IllegalArgumentException.
	 * 
	 * @param name - name of the player
	 */	
	public NullSimplePlayer(String name)
	{
		super("", name, 0);
		this.id =  null;
	};
	
	/**
	 * A NullSimplePlayer cannot have their points set.
	 * 
	 * @param points - discarded
	 */
	@Override
	public void setPoints(int points)
	{
		;
	}

	/**
	 * A NullSimplePlayer cannot have their bet set. Returns false.
	 * 
	 * @param bet - discarded
	 * @return false
	 */
	@Override
	public boolean setBet(int bet)
	{
		return false;
	}
	
	/**
	 * Throws an IllegalArgumentException. NullSimplePlayer's ID is always null, and thus cannot
	 * be compared.
	 * 
	 * @param player - discarded
	 * @throws IllegalArgumentException is always thrown.
	 * @return does not return, always throws IllegalArgumentException
	 */
	@Override
	public int compareTo(Player player)
	{
		throw new IllegalArgumentException("Attempting to compare Player with null ID.");
	}
}
