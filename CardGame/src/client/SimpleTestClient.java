package client;

import java.util.Deque;
import java.util.logging.Level;

import model.GameEngineImpl;
import model.SimplePlayer;
import model.interfaces.GameEngine;
import model.interfaces.Player;
import model.interfaces.PlayingCard;
import validate.Validator;
import view.GameEngineCallbackImpl;
import view.interfaces.GameEngineCallback;

/**
 * Simple console client for FP assignment 2, 2020
 * NOTE: This code will not compile until you have implemented code for the supplied interfaces!
 * 
 * You must be able to compile your code WITHOUT modifying this class.
 * Additional testing should be done by copying and adding to this class while ensuring this class still works.
 * 
 * The provided Validator.jar will check if your code adheres to the specified interfaces!
 * 
 * @author Caspar Ryan
 * 
 */
public class SimpleTestClient
{
	public static void main(String args[])
	{
		final GameEngine gameEngine = new GameEngineImpl();

		// call method in Validator.jar to test *structural* correctness
		// just passing this does not mean it actually works .. you need to test yourself!
		// pass false if you want to disable logging .. (i.e. once it passes)
		Validator.validate(false);

		// create test players
		Player[] players = new Player[] {
				new SimplePlayer("2", "The Shark", 500),
				new SimplePlayer("1", "The Loser", 500),
				new SimplePlayer("3", "The Apple", 500),
				new SimplePlayer("4", "The Broke", 500),
				new SimplePlayer("b", "The Broker", 500),
				new SimplePlayer("c", "The Counter", 500),
				new SimplePlayer("d", "The Desk", 500),
				new SimplePlayer("e", "The Etch", 500),
				new SimplePlayer("f", "The Furthest", 500),
				new SimplePlayer("g", "The Grape", 500),
				new SimplePlayer("10", "The Hotel", 500),
				new SimplePlayer("11", "The India", 500),
				new SimplePlayer("12", "The Joke", 500),
				new SimplePlayer("13", "The Kremlin", 500),
				new SimplePlayer("14", "The Most", 500),
		};

		for (Player player : players)
		{
			gameEngine.addPlayer(player);
		}


		// add logging callback

		GameEngineCallback gec = new GameEngineCallbackImpl();
		gameEngine.addGameEngineCallback(gec);

		// Uncomment this to DEBUG your deck of cards creation
		//      Deque<PlayingCard> shuffledDeck = gameEngine.getShuffledHalfDeck();
		//      printCards(shuffledDeck);

		// main loop to add players, place a bet and receive hand
		for (Player player : players)
		{
			gameEngine.placeBet(player, 100);
			gameEngine.dealPlayer(player, 0);
		}

		// all players have played so now house deals 
		// GameEngineCallBack.houseResult() is called to log all players (after results are calculated)
		gameEngine.dealHouse(0);
	}

	@SuppressWarnings("unused")
	private static void printCards(Deque<PlayingCard> deck)
	{
		for (PlayingCard card : deck)
			GameEngineCallbackImpl.logger.log(Level.INFO, card.toString());
	}
}
