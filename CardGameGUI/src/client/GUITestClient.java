package client;

import javax.swing.SwingUtilities;

import model.GameEngineImpl;
import model.interfaces.GameEngine;
import view.GameEngineCallbackImpl;
import view.MainFrame;
import view.interfaces.GameEngineCallback;

/**
 * Simple awt/swing client for FP assignment 2, 2020
 * 
 * @author Michael Asquith
 */
public class GUITestClient
{
	public static void main(String args[]) {
		final GameEngine gameEngine = new GameEngineImpl();

		GameEngineCallback consoleGEC = new GameEngineCallbackImpl();
		gameEngine.addGameEngineCallback(consoleGEC);

		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run() {
				String gameName = "TOTALLY REAL POKER-LIKE GAME";
				new MainFrame(gameName, gameEngine);
			}
		});
	}
}
