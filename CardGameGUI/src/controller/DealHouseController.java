package controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import view.interfaces.UnifiedGUIModel;
import view.model.UnifiedGUIModelImpl;

/**
 * Waits for the ALL_PLAYERS_DEALT property to be fired, then calls the view model to deal for
 * the house.
 * 
 * @author Michael Asquith
 */
public class DealHouseController implements PropertyChangeListener
{
	private final String MESSAGE = "All players have been dealt. Ready to deal house.";
	private UnifiedGUIModelImpl guiModel;
	private JFrame owner;

	public DealHouseController(JFrame owner, UnifiedGUIModelImpl guiModel) {
		this.owner = owner;
		this.guiModel = guiModel;
		guiModel.addPropertyChangeListener(this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		// If all players have been dealt, and there is at least one player...
		if (evt.getPropertyName() == UnifiedGUIModel.PROPERTY_ALL_PLAYERS_DEALT
				&& (boolean) evt.getNewValue() && guiModel.getAllPlayers().size() != 0) {

			JOptionPane.showMessageDialog(owner, MESSAGE);
			guiModel.setCurrentPlayer(UnifiedGUIModel.DEALER);
			guiModel.dealHouse(100);
		}
	}

}
