package controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.interfaces.Player;
import view.interfaces.UnifiedGUIModel;

/**
 * Launches a message dialog when a player has left the game due to having 0 points.
 * 
 * @author Michael Asquith
 */
public class PlayerLostController implements PropertyChangeListener
{
	private final String message = "%s has been eliminated";
	private JFrame owner;

	public PlayerLostController(JFrame owner, UnifiedGUIModel guiModel) {
		this.owner = owner;
		guiModel.addPropertyChangeListener(this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {

		if (evt.getPropertyName() == UnifiedGUIModel.PROPERTY_PLAYER_LOST) {
			Player player = (Player) evt.getOldValue();
			JOptionPane.showMessageDialog(owner, String.format(message, player.getPlayerName()));
		}
	}

}
