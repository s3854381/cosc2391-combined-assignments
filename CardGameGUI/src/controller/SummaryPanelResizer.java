package controller;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import view.centrepanel.CentrePanel;

/**
 * When the CentrePanel is resized, calls that CentrePanel's method to resize its components.
 * 
 * @author Michael Asquith
 */
public class SummaryPanelResizer extends ComponentAdapter
{
	private CentrePanel panel;

	public SummaryPanelResizer(CentrePanel panel) { this.panel = panel; }

	@Override
	public void componentResized(ComponentEvent e) { panel.resizeComponents(); }
}
