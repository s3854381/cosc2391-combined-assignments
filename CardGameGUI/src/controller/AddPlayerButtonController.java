package controller;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.menubar.AddPlayerDialog;
import view.model.UnifiedGUIModelImpl;

/**
 * Launches an AddPlayerDialog in response to the add player action.
 * 
 * @author Michael Asquith
 */
public class AddPlayerButtonController implements ActionListener
{
	private Frame frame;
	private UnifiedGUIModelImpl guiModel;
	private static int ID_COUNTER = 0;

	public AddPlayerButtonController(Frame frame, UnifiedGUIModelImpl guiModel) {
		this.guiModel = guiModel;
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		new AddPlayerDialog(frame, guiModel, String.valueOf(ID_COUNTER));
		++ID_COUNTER;
	}

}
