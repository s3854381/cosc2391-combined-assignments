package controller;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import model.interfaces.Player;
import view.model.UnifiedGUIModelImpl;

/**
 * Removes a player from the game, after they have confirmed their action through a dialog
 * box. DESIGN NOTE: The statically-called confirm dialog did not seem sufficient to require
 * its own class in the view package.
 * 
 * @author Michael Asquith
 */
public class RemovePlayerController implements ActionListener
{
	private Frame frame;
	private UnifiedGUIModelImpl guiModel;
	private String message;

	public RemovePlayerController(Frame frame, UnifiedGUIModelImpl guiModel) {
		this.guiModel = guiModel;
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Player currentPlayer = guiModel.getCurrentPlayer();
		message = String.format("Are you sure you want to remove %s from the game?",
				currentPlayer.getPlayerName());
		int n = JOptionPane.showConfirmDialog(frame, message, "Confirm Remove Player",
				JOptionPane.YES_NO_OPTION);
		if (n == JOptionPane.YES_OPTION) guiModel.removePlayer(currentPlayer);
	}

}
