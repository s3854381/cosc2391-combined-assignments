package controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JOptionPane;

import view.interfaces.UnifiedGUIModel;
import view.toolbar.BetDialog;

/**
 * Responds to the output from a BetDialog, setting a player's bet in the view model.
 * 
 * @author Michael Asquith
 */
public class BetDialogController implements PropertyChangeListener
{
	private BetDialog dialog;
	private UnifiedGUIModel guiModel;

	public BetDialogController(UnifiedGUIModel guiModel, BetDialog dialog) {
		this.dialog = dialog;
		this.guiModel = guiModel;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String propertyName = evt.getPropertyName();
		if (dialog.isVisible() && (propertyName.equals(JOptionPane.VALUE_PROPERTY)
				|| propertyName.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {

			JOptionPane optionPane = dialog.getOptionPane();
			Object value = optionPane.getValue();

			if (!value.equals(JOptionPane.UNINITIALIZED_VALUE)) {
				optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

				if (value.equals(dialog.getBtnString1())) {
					boolean isInt = true;
					boolean correctRange = true;
					try {
						int bet = Integer.parseInt(dialog.getBet());
						correctRange = guiModel.placeBet(guiModel.getCurrentPlayer(), bet);
					}
					catch (NumberFormatException e) {
						isInt = false;
					}

					if (isInt && correctRange) {
						dialog.dispose();
					}
					else {
						dialog.displayerErrorMessage(isInt, correctRange);
					}
				}
				else {
					dialog.dispose();
				}
			}
		}
	}

}
