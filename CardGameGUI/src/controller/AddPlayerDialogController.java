package controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JOptionPane;

import model.SimplePlayer;
import model.interfaces.Player;
import view.interfaces.UnifiedGUIModel;
import view.menubar.AddPlayerDialog;

/**
 * Responds to the output of an AddPlayerDialog, adding a new player to the view model.
 * 
 * @author Michael Asquith
 */
public class AddPlayerDialogController implements PropertyChangeListener
{
	private AddPlayerDialog dialog;
	private UnifiedGUIModel guiModel;

	public AddPlayerDialogController(UnifiedGUIModel guiModel, AddPlayerDialog dialog) {
		this.dialog = dialog;
		this.guiModel = guiModel;
	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		String propertyName = arg0.getPropertyName();
		if (dialog.isVisible() && (propertyName.equals(JOptionPane.VALUE_PROPERTY)
				|| propertyName.equals(JOptionPane.INPUT_VALUE_PROPERTY))) {

			JOptionPane optionPane = dialog.getOptionPane();
			Object value = optionPane.getValue();

			if (!value.equals(JOptionPane.UNINITIALIZED_VALUE)) {
				optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

				if (value.equals(dialog.getBtnString1())) {

					String name = dialog.getName();

					boolean pointsAreInt = true;
					boolean pointsArePositive = true;
					boolean isNewName = !(name.equals(guiModel.getDealerName()));
					boolean nameLongEnough = name.length() > 0;
					int points = 0;
					try {
						points = Integer.parseInt(dialog.getPoints());
						pointsArePositive = points > 0;
					}
					catch (NumberFormatException e) {
						pointsAreInt = false;
					}
					for (Player player : guiModel.getAllPlayers()) {
						isNewName = isNewName && !(name.equals(player.getPlayerName()));
					}

					if (pointsAreInt && pointsArePositive && nameLongEnough && isNewName) {
						guiModel.addPlayer(new SimplePlayer(dialog.getNewID(), name, points));
						dialog.dispose();
					}
					else {
						dialog.displayErrorMessage(nameLongEnough, isNewName, pointsAreInt,
								pointsArePositive);
					}
				}
				else {
					dialog.dispose();
				}
			}
		}
	}

}
