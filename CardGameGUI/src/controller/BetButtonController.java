package controller;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.model.UnifiedGUIModelImpl;
import view.toolbar.BetDialog;

/**
 * Launches a BetDialog in response to the bet action.
 * 
 * @author Michael Asquith
 */
public class BetButtonController implements ActionListener
{
	private UnifiedGUIModelImpl guiModel;
	private Frame frame;

	public BetButtonController(Frame frame, UnifiedGUIModelImpl guiModel) {
		this.frame = frame;
		this.guiModel = guiModel;
	};

	@Override
	public void actionPerformed(ActionEvent arg0) { new BetDialog(frame, guiModel); }

}
