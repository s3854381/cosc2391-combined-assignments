package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.model.UnifiedGUIModelImpl;

/**
 * Responds to the deal player action, informing the view model to deal for the player.
 * 
 * @author Michael Asquith
 */
public class DealPlayerController implements ActionListener
{
	private UnifiedGUIModelImpl guiModel;

	public DealPlayerController(UnifiedGUIModelImpl guiModel) { this.guiModel = guiModel; }

	@Override
	public void actionPerformed(ActionEvent arg0) {
		guiModel.dealPlayer(guiModel.getCurrentPlayer(), 100);
	}

}
