package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.interfaces.UnifiedGUIModel;

/**
 * Clears a bet in response to the clear bet action.
 * 
 * @author Michael Asquith
 */
public class ClearBetController implements ActionListener
{
	private UnifiedGUIModel guiModel;

	public ClearBetController(UnifiedGUIModel guiModel) { this.guiModel = guiModel; }

	@Override
	public void actionPerformed(ActionEvent arg0) { guiModel.resetBet(guiModel.getCurrentPlayer()); }

}
