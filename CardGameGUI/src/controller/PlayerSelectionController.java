package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.interfaces.UnifiedGUIModel;
import view.toolbar.PlayerSelectionBox;

/**
 * Responds to the player selection action, setting the selected player in the view model.
 * 
 * @author Michael Asquith
 */
public class PlayerSelectionController implements ActionListener
{
	private UnifiedGUIModel guiModel;
	PlayerSelectionBox selectionBox;

	public PlayerSelectionController(UnifiedGUIModel guiModel, PlayerSelectionBox selectionBox) {
		this.guiModel = guiModel;
		this.selectionBox = selectionBox;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		guiModel.setCurrentPlayer(selectionBox.getCurrentPlayer());
	}

}
