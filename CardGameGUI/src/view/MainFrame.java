package view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JMenuBar;

import controller.DealHouseController;
import controller.PlayerLostController;
import model.interfaces.GameEngine;
import view.centrepanel.CentrePanel;
import view.menubar.PlayerMenuBar;
import view.model.UnifiedGUIModelImpl;
import view.statusbar.StatusBar;
import view.toolbar.GameToolBar;

/**
 * Main frame of the game.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame
{
	public MainFrame(String windowName, GameEngine gameEngine) {
		super(windowName);

		UnifiedGUIModelImpl guiModel = new UnifiedGUIModelImpl(gameEngine);

		// Floating controllers
		new DealHouseController(this, guiModel);
		new PlayerLostController(this, guiModel);

		// Starting frame size and location
		int x = 200;
		int y = 200;
		int width = 800;
		int height = 600;
		setBounds(x, y, width, height);

		setLayout(new BorderLayout());

		GameToolBar gameToolBar = new GameToolBar(this, guiModel);
		add(gameToolBar, BorderLayout.NORTH);

		CentrePanel centrePanel = new CentrePanel(this, guiModel);
		add(centrePanel, BorderLayout.CENTER);

		StatusBar statusBar = new StatusBar(guiModel);
		add(statusBar, BorderLayout.SOUTH);

		JMenuBar menuBar = new PlayerMenuBar(this, guiModel);
		setJMenuBar(menuBar);

		gameEngine.addGameEngineCallback(
				new GameEngineCallbackGUI(this, guiModel, centrePanel.getGameMat()));

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setVisible(true);
	}
}
