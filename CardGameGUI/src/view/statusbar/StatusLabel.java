package view.statusbar;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.border.LineBorder;

/**
 * Blank status label to go on the status bar.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class StatusLabel extends JLabel
{
	public StatusLabel(String text, int horizontalAlignment) {
		super(text, horizontalAlignment);
		setBorder(new LineBorder(Color.GRAY));
	}
}
