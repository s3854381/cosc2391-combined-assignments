package view.statusbar;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import view.interfaces.UnifiedGUIModel;

/**
 * Status bar with one label displaying the current player, one one empty label.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class StatusBar extends JPanel
{
	public StatusBar(UnifiedGUIModel guiModel) {
		setLayout(new GridLayout(1, 2));
		add(new PlayerSelectedLabel(JLabel.LEFT, guiModel));
		add(new StatusLabel("", JLabel.RIGHT));
	}
}
