package view.statusbar;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import view.interfaces.UnifiedGUIModel;

/**
 * Simple label displaying the currently selected player on the status bar.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class PlayerSelectedLabel extends StatusLabel implements PropertyChangeListener
{
	private UnifiedGUIModel guiModel;

	public PlayerSelectedLabel(int horizontalAlignment, UnifiedGUIModel guiModel) {
		super("", horizontalAlignment);
		this.guiModel = guiModel;
		guiModel.addPropertyChangeListener(this);
		updateText();
	}

	private void updateText() {
		setText(String.format(" %s is selected.", guiModel.getCurrentPlayerName()));
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName() == UnifiedGUIModel.PROPERTY_CURRENT_PLAYER) { updateText(); }
	}
}
