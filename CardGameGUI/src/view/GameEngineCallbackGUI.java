/**
 * 
 */
package view;

import java.awt.Frame;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import model.interfaces.GameEngine;
import model.interfaces.Player;
import model.interfaces.PlayingCard;
import view.centrepanel.GameMat;
import view.interfaces.GameEngineCallback;
import view.interfaces.PlayerTable;
import view.interfaces.UnifiedGUIModel;

/**
 * Calls methods on view to update things. I.e. GameMat and the guiModel. Because this has
 * come from the GameEngine, which does not work off the Swing Event Thread, all methods
 * immediately invokeLater() to return to that thread.
 * 
 * @author Michael Asquith
 */
public class GameEngineCallbackGUI implements GameEngineCallback
{
	private Frame frame;
	private UnifiedGUIModel guiModel;
	private GameMat gameMat;

	public GameEngineCallbackGUI(Frame frame, UnifiedGUIModel guiModel, GameMat gameMat) {
		this.frame = frame;
		this.guiModel = guiModel;
		this.gameMat = gameMat;
	}

	@Override
	public void nextCard(Player player, PlayingCard card, GameEngine engine) {
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run() { gameMat.drawCard(player, card); }
		});
	}

	@Override
	public void bustCard(Player player, PlayingCard card, GameEngine engine) {

		// Do nothing
	}

	@Override
	public void result(Player player, int result, GameEngine engine) {
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run() { guiModel.finishDealingPlayer(player, result); }
		});

	}

	@Override
	public void nextHouseCard(PlayingCard card, GameEngine engine) {
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run() { gameMat.drawCard(UnifiedGUIModel.DEALER, card); }
		});
	}

	@Override
	public void houseBustCard(PlayingCard card, GameEngine engine) {

		// Do nothing
	}

	@Override
	public void houseResult(int houseResult, GameEngine engine) {
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run() {
				for (Player player : engine.getAllPlayers()) {

					int playerResult = player.getResult();
					PlayerTable.WinLossResult playerWinLoss;

					if (playerResult > houseResult) {
						playerWinLoss = PlayerTable.WinLossResult.WIN;
					}
					else if (playerResult < houseResult) {
						playerWinLoss = PlayerTable.WinLossResult.LOSS;
					}
					else {
						playerWinLoss = PlayerTable.WinLossResult.DRAW;
					}

					guiModel.gameIsFinished(player, playerWinLoss);
				}
				guiModel.finishedDealingHouse();

				String message = String.format("The House scored %d. Would you like to play again?",
						houseResult);

				int playAgain = JOptionPane.showConfirmDialog(frame, message, "Game Over",
						JOptionPane.YES_NO_OPTION);
				if (playAgain == JOptionPane.NO_OPTION) { frame.dispose(); }
			}
		});

	}
}
