package view.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.DefaultTableModel;

import model.interfaces.Player;
import view.interfaces.PlayerTable;

/**
 * A table of player information. Stores public data in its TableModel, and private data in
 * the dealtMap.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class PlayerTableModel extends DefaultTableModel implements PlayerTable
{
	private List<Player> players = new ArrayList<>();
	private Map<Player, Boolean> dealtMap = new HashMap<>();

	/**
	 * Visible table columns, with an overridden toString() method.
	 * @author Michael Asquith
	 */
	public enum Column {
		PLAYER_NAME("Player name"),
		POINTS("Points"),
		BET("Bet"),
		RESULT("Result"),
		WIN_LOSS("Win/Loss"),
		LAST_WINNINGS("Last Winnings");

		private String label;

		Column(String label) { this.label = label; }

		@Override
		public String toString() { return this.label; }
	}

	public PlayerTableModel() {

		super();
		for (Column c : Column.values()) {
			addColumn(c.toString());
		}
	}

	@Override
	public boolean hasDealt(Player player) { return dealtMap.get(player); }

	@Override
	public void setHasDealt(Player player, boolean hasDealt) { dealtMap.put(player, hasDealt); }

	@Override
	public void addPlayer(Player player) {
		int playerIndex = getPlayerIndex(player);
		if (playerIndex != -1) { players.remove(playerIndex); removeRow(playerIndex); }

		Object[] rowData = { player.getPlayerName(), player.getPoints(), player.getBet(),
				player.getResult(), PlayerTable.WinLossResult.DID_NOT_PLAY, 0 };

		dealtMap.put(player, false);
		players.add(player);
		addRow(rowData);
	}

	@Override
	public void removePlayer(Player player) {
		removeRow(getPlayerIndex(player));
		players.remove(player);
		dealtMap.remove(player);
	}

	@Override
	public void betIsSet(Player player) { setPlayerValue(player.getBet(), player, Column.BET); }

	@Override
	public void setResult(Player player, int result) { setPlayerValue(result, player, Column.RESULT); }

	@Override
	public void gameIsFinished(Player player, PlayerTable.WinLossResult winLossResult) {
		int previousPoints = (int) getValueAt(getPlayerIndex(player),
				findColumn(Column.POINTS.toString()));

		setPlayerValue(player.getPoints() - previousPoints, player, Column.LAST_WINNINGS);
		setPlayerValue(player.getPoints(), player, Column.POINTS);
		setPlayerValue(winLossResult, player, Column.WIN_LOSS);
		setPlayerValue(0, player, Column.BET);
		setHasDealt(player, false);
	}

	private void setPlayerValue(Object value, Player player, Column column) {
		int playerIndex = getPlayerIndex(player);
		int columnIndex = findColumn(column.toString());
		setValueAt(value, playerIndex, columnIndex);
	}

	private int getPlayerIndex(Player player) { return players.indexOf(player); }
}
