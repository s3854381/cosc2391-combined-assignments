package view.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Deque;

import javax.swing.table.TableModel;

import model.interfaces.GameEngine;
import model.interfaces.Player;
import model.interfaces.PlayingCard;
import view.interfaces.GameEngineCallback;
import view.interfaces.PlayerTable;
import view.interfaces.UnifiedGUIModel;

/**
 * Combined GameEngine and ViewModel. When controllers require the model to change, the
 * UnifiedGUIModel takes the call. If the call is to a GameEngine, the GUIModel sends its
 * delegate. As the overarching ViewModel, it should be observable for any changes in the
 * ViewModel (Model observations are handled by the GameEngineCallbackGUI).
 * 
 * @author Michael Asquith
 */
public class UnifiedGUIModelImpl implements UnifiedGUIModel
{
	private PropertyChangeSupport propertySupport = new PropertyChangeSupport(this);
	private Player currentPlayer = DEALER;
	private final String dealerName = "The House";
	private final PlayerTableModel playerTable;
	private final GameEngine gameEngine;
	private boolean isDealing = false;

	public UnifiedGUIModelImpl(GameEngine gameEngine) {

		this.playerTable = new PlayerTableModel();

		this.gameEngine = gameEngine;
	}

	private boolean currentPlayerDealt() { return playerTable.hasDealt(currentPlayer); }
	
	private void checkIfHouseReady() {
		boolean allPlayersDealt = true;
		for (Player p : gameEngine.getAllPlayers()) {
			allPlayersDealt = allPlayersDealt && playerTable.hasDealt(p);
		}

		if (allPlayersDealt) propertySupport.firePropertyChange(PROPERTY_ALL_PLAYERS_DEALT, false, true);
	}

	private boolean removePlayerImpl(Player player) {
		boolean success = gameEngine.removePlayer(player);
		if (success) {

			if (getCurrentPlayer() == player) setCurrentPlayer(DEALER);
			propertySupport.firePropertyChange(PROPERTY_PLAYER_REMOVED, player, null);
			playerTable.removePlayer(player);
		}
		return success;
	}

	@Override
	public void setCurrentPlayer(Player player) {
		Player oldPlayer = currentPlayer;
		boolean oldDealReady = readyToDeal();
		boolean oldBetReady = readyToBet();
		boolean oldClearReady = readyToClearBet();
		currentPlayer = player;
		propertySupport.firePropertyChange(PROPERTY_CURRENT_PLAYER, oldPlayer, currentPlayer);
		propertySupport.firePropertyChange(PROPERTY_READY_TO_DEAL, oldDealReady, readyToDeal());
		propertySupport.firePropertyChange(PROPERTY_READY_TO_BET, oldBetReady, readyToBet());
		propertySupport.firePropertyChange(PROPERTY_READY_TO_CLEAR_BET, oldClearReady, readyToClearBet());

	}

	@Override
	public Player getCurrentPlayer() { return currentPlayer; }

	@Override
	public boolean dealerSelected() { return currentPlayer == DEALER; }

	@Override
	public String getCurrentPlayerName() {
		String returnValue = dealerName;
		if (currentPlayer != DEALER) returnValue = currentPlayer.getPlayerName();
		return returnValue;
	}
	
	@Override
	public String getDealerName() {
		return dealerName;
	}

	@Override
	public boolean readyToClearBet() { return readyToBet() && currentPlayer.getBet() > 0; }

	/**
	 * Checks if the current player is ready to be dealt to.
	 * 
	 * @return True if the current player is ready to be dealt to.
	 */
	@Override
	public boolean readyToDeal() { return readyToClearBet() && !isDealing; }

	@Override
	public boolean readyToBet() { return !dealerSelected() && !currentPlayerDealt(); }

	@Override
	public TableModel getPlayerTable() { return playerTable; }

	@Override
	public void gameIsFinished(Player player, PlayerTable.WinLossResult playerWinLoss) {
		playerTable.gameIsFinished(player, playerWinLoss);
		
		if (player.getPoints() == 0) {
			propertySupport.firePropertyChange(PROPERTY_PLAYER_LOST, player, null);
			removePlayerImpl(player);
		}
	}

	@Override
	public void resetBet(Player player) {
		player.resetBet();
		playerTable.betIsSet(player);
		propertySupport.firePropertyChange(PROPERTY_READY_TO_DEAL, true, false);
	}
	
	@Override
	public void finishDealingPlayer(Player player, int result) {
		playerTable.setResult(player, result);
		isDealing = false;
		checkIfHouseReady();
		propertySupport.firePropertyChange(PROPERTY_READY_TO_DEAL, false, readyToDeal());
	}
	
	@Override
	public void finishedDealingHouse() {
		propertySupport.firePropertyChange(PROPERTY_ALL_PLAYERS_DEALT, true, false);
	}

	@Override
	public void addPlayer(Player player) {
		gameEngine.addPlayer(player);
		playerTable.addPlayer(player);
		propertySupport.firePropertyChange(PROPERTY_NEW_PLAYER, null, player);
		setCurrentPlayer(player);
	}

	@Override
	public boolean removePlayer(Player player) {
		boolean success = removePlayerImpl(player);
		if (success) checkIfHouseReady();
		return success;
	}

	@Override
	public boolean placeBet(Player player, int bet) {
		boolean oldDealReady = readyToDeal();
		boolean oldClearReady = readyToClearBet();
		boolean success = gameEngine.placeBet(player, bet);
		if (success) {
			playerTable.betIsSet(player);
			propertySupport.firePropertyChange(PROPERTY_READY_TO_CLEAR_BET, oldClearReady, readyToClearBet());
			propertySupport.firePropertyChange(PROPERTY_READY_TO_DEAL, oldDealReady, readyToDeal());
		}
		return success;
	}

	@Override
	public void dealPlayer(Player player, int delay) throws IllegalArgumentException {
		isDealing = true;
		playerTable.setHasDealt(player, true);
		propertySupport.firePropertyChange(PROPERTY_READY_TO_CLEAR_BET, true, false);
		propertySupport.firePropertyChange(PROPERTY_READY_TO_DEAL, true, false);
		propertySupport.firePropertyChange(PROPERTY_READY_TO_BET, true, false);
		propertySupport.firePropertyChange(PROPERTY_PLAYER_DEALT, null, player);

		new Thread()
		{
			@Override
			public void run() {
				gameEngine.dealPlayer(player, delay);
			}
		}.start();
	}

	@Override
	public void dealHouse(int delay) throws IllegalArgumentException {

	propertySupport.firePropertyChange(PROPERTY_PLAYER_DEALT, null, DEALER);
		
		new Thread()
		{
			@Override
			public void run() {
				gameEngine.dealHouse(delay);
			}
		}.start();

	}

	@Override
	public void applyWinLoss(Player player, int houseResult) {
		gameEngine.applyWinLoss(player, houseResult);
	}

	@Override
	public Player getPlayer(String id) { return gameEngine.getPlayer(id); }

	@Override
	public void addGameEngineCallback(GameEngineCallback gameEngineCallback) {
		gameEngine.addGameEngineCallback(gameEngineCallback);
	}

	@Override
	public boolean removeGameEngineCallback(GameEngineCallback gameEngineCallback) {
		return gameEngine.removeGameEngineCallback(gameEngineCallback);
	}

	@Override
	public Collection<Player> getAllPlayers() { return gameEngine.getAllPlayers(); }

	@Override
	public Deque<PlayingCard> getShuffledHalfDeck() { return gameEngine.getShuffledHalfDeck(); }

	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		propertySupport.addPropertyChangeListener(l);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		propertySupport.removePropertyChangeListener(l);
	}
}
