package view.menubar;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.AddPlayerDialogController;
import view.model.UnifiedGUIModelImpl;

/**
 * Dialog box to get the details of a new player to be added to the game. Based off the Java
 * Tutorial's custom dialog box at:
 * https://docs.oracle.com/javase/tutorial/displayCode.html?code=https://docs.oracle.com/javase/tutorial/uiswing/examples/components/DialogDemoProject/src/components/CustomDialog.java
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class AddPlayerDialog extends JDialog implements ActionListener
{

	private final String defaultName = "Ford Prefect";
	private final String defaultPoints = "1000";
	private final String frameTitle = "New Player";
	private final String question1 = "Enter the player's name:";
	private final String question2 = "Enter the player's starting points:";
	private final String btnString1 = "Ok";
	private final String btnString2 = "Cancel";

	private JOptionPane optionPane;
	private JTextField nameField;
	private JTextField pointsField;
	private String newID;

	private static final Dimension INITIAL_SIZE = new Dimension(300, 200);

	public AddPlayerDialog(Frame frame, UnifiedGUIModelImpl guiModel, String playerID) {

		super(frame, true);
		this.newID = playerID;
		setTitle(frameTitle);
		setSize(INITIAL_SIZE);
		this.setLocation(0, 0);

		int frameX = frame.getX();
		int frameY = frame.getY();
		int frameWidth = frame.getWidth();
		int frameHeight = frame.getHeight();
		int thisX = frameX + frameWidth / 2 - getWidth() / 2;
		int thisY = frameY + frameHeight / 2 - getHeight() / 2;
		this.setLocation(thisX, thisY);

		nameField = new JTextField(defaultName);
		pointsField = new JTextField(defaultPoints);
		Object[] array = { question1, nameField, question2, pointsField };

		Object[] options = { btnString1, btnString2 };

		optionPane = new JOptionPane(array, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION,
				null, options, options[0]);

		setContentPane(optionPane);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		// Ensure the text field always gets the first focus.
		addComponentListener(new ComponentAdapter()
		{
			public void componentShown(ComponentEvent ce) { nameField.requestFocusInWindow(); }
		});

		// Register an event handler that puts the text into the option pane.
		nameField.addActionListener(this);
		pointsField.addActionListener(this);

		// Register an event handler that reacts to option pane state changes.
		optionPane.addPropertyChangeListener(new AddPlayerDialogController(guiModel, this));

		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) { optionPane.setValue(btnString1); }

	public JOptionPane getOptionPane() { return optionPane; }

	public String getBtnString1() { return btnString1; }

	public String getName() { return nameField.getText(); }

	public String getPoints() { return pointsField.getText(); }

	public String getNewID() { return newID; }

	public void displayErrorMessage(boolean nameLongEnough, boolean isNewName, boolean pointsAreInt,
			boolean pointsArePositive) {
		String errorMessage;
		if (!nameLongEnough) {
			errorMessage = "Please enter a name.";
			nameField.selectAll();
		}
		else if (!isNewName) {
			errorMessage = String.format("Sorry, the name %s is already taken. Please enter a new name.",
					getName());
			nameField.selectAll();
		}
		else if (!pointsAreInt || !pointsArePositive) {
			errorMessage = "Please enter a positive whole number for starting points.";
			pointsField.selectAll();
		}
		else {
			errorMessage = "Something strange happened. Try again.";
		}
		JOptionPane.showMessageDialog(this, errorMessage, "Try again", JOptionPane.ERROR_MESSAGE);
		if (!nameLongEnough || !isNewName) {
			nameField.requestFocusInWindow();
		}
		else {
			pointsField.requestFocusInWindow();
		}

	}
}
