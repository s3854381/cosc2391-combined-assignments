package view.menubar;

import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import com.sun.glass.events.KeyEvent;

import controller.AddPlayerButtonController;
import controller.RemovePlayerController;
import view.interfaces.UnifiedGUIModel;
import view.model.UnifiedGUIModelImpl;

/**
 * Menu bar containing the add player and remove player options.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class PlayerMenuBar extends JMenuBar implements PropertyChangeListener
{
	private JMenuItem addPlayer;
	private JMenuItem removePlayer;
	private UnifiedGUIModel guiModel;

	public PlayerMenuBar(Frame frame, UnifiedGUIModelImpl guiModel) {
		this.guiModel = guiModel;

		JMenu playerMenu = new JMenu("Player");
		playerMenu.setMnemonic(KeyEvent.VK_P);
		guiModel.addPropertyChangeListener(this);

		addPlayer = new JMenuItem("Add player...", KeyEvent.VK_A);
		removePlayer = new JMenuItem("Remove current player...", KeyEvent.VK_R);

		addPlayer.addActionListener(new AddPlayerButtonController(frame, guiModel));
		removePlayer.addActionListener(new RemovePlayerController(frame, guiModel));

		playerMenu.add(addPlayer);
		playerMenu.add(removePlayer);

		removePlayer.setEnabled(false);

		add(playerMenu);
	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		if (arg0.getPropertyName() == UnifiedGUIModel.PROPERTY_CURRENT_PLAYER) {
			removePlayer.setEnabled(!guiModel.dealerSelected());
		}
	}
}
