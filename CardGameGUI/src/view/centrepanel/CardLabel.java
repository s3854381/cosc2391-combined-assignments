package view.centrepanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JLabel;

import model.interfaces.PlayingCard;

/**
 * JLabel that displays a playing card.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class CardLabel extends JLabel
{
	private static final char[] CARD_CHARS = { 'A', 'K', 'Q', 'J', 'T', '9', '8' };
	private static Map<PlayingCard.Value, Character> charMap = null;

	private static final double POKER_CARD_HEIGHT_WIDTH_RATIO = 3.5 / 2.25;
	private static final String PATH = "img/%s.png";
	private PlayingCard card = null;
	BufferedImage suitIcon = null;

	public CardLabel() {

		if (charMap == null) {
			charMap = new HashMap<>();

			for (int i = 0; i < CARD_CHARS.length; ++i) {
				charMap.put(PlayingCard.Value.values()[i], CARD_CHARS[i]);
			}
		}
	}

	/**
	 * Sets the label's current card to display. If the PlayingCard is null, then nothing will
	 * display.
	 * 
	 * @param card
	 */
	public void setCard(PlayingCard card) {
		this.card = card;
		if (card == null) {
			suitIcon = null;
		}
		else {
			String filename = String.format(PATH, card.getSuit().toString().toLowerCase());
			try {
				suitIcon = ImageIO.read(new File(filename));
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (card != null) {

			// Card base
			int cardHeight = (int) (this.getWidth() * POKER_CARD_HEIGHT_WIDTH_RATIO);
			int cardWidth = getWidth();
			int cornerArcSize = cardWidth / 9;
			int cardY = getHeight() / 2 - cardHeight / 2;

			g.setColor(Color.WHITE);
			g.fillRoundRect(0, cardY, cardWidth, cardHeight, cornerArcSize, cornerArcSize);

			// Suit symbol in the middle of the card
			int suitSize = getWidth() / 5;
			Image scaledSuit = suitIcon.getScaledInstance(suitSize, suitSize, Image.SCALE_SMOOTH);
			int suitX = cardWidth / 2 - scaledSuit.getWidth(null) / 2;
			int suitY = cardY + cardHeight / 2 - scaledSuit.getHeight(null) / 2;

			g.drawImage(scaledSuit, suitX, suitY, null);

			// Card's value string (one character) in the top left and bottom right corners.
			char[] c = { charMap.get(card.getValue()) };
			PlayingCard.Suit suit = card.getSuit();
			if (suit == PlayingCard.Suit.HEARTS || suit == PlayingCard.Suit.DIAMONDS) {
				g.setColor(Color.RED);
			}
			else {
				g.setColor(Color.BLACK);
			}
			int fontSize = cardWidth / 6;
			g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, fontSize));

			int topCharX = cardWidth / 18;
			int bottomCharX = cardWidth - cardWidth / 6;
			int topCharY = cardWidth / 6;
			int bottomCharY = cardY + cardHeight - cardWidth / 12;

			g.drawChars(c, 0, 1, topCharX, cardY + topCharY);
			g.drawChars(c, 0, 1, bottomCharX, bottomCharY);
		}
	}
}
