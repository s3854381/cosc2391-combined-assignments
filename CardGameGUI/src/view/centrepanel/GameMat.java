package view.centrepanel;

import java.awt.CardLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import model.interfaces.Player;
import model.interfaces.PlayingCard;
import view.interfaces.UnifiedGUIModel;

/**
 * Mat the cards are played on. Contains a "card" for each player, and switches to a player's
 * card when that player is selected.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class GameMat extends JPanel implements PropertyChangeListener
{
	private Map<Player, HandPanel> handMap = new HashMap<>();
	private final String dealerHandString = "";
	private UnifiedGUIModel guiModel;
	private CardLayout layout;

	public GameMat(UnifiedGUIModel guiModel) {
		this.guiModel = guiModel;
		guiModel.addPropertyChangeListener(this);
		layout = new CardLayout();
		this.setLayout(layout);

		HandPanel dealerHand = new HandPanel();
		add(dealerHand, dealerHandString);
		handMap.put(null, dealerHand);
	}

	private void addPlayer(Player player) {
		HandPanel hand = new HandPanel();
		add(hand, player.getPlayerName());
		handMap.put(player, hand);
	}

	private void removePlayer(Player player) {
		HandPanel hand = handMap.get(player);
		remove(hand);
		handMap.remove(player);
	}

	public void drawCard(Player player, PlayingCard card) { handMap.get(player).addCard(card); }

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String property = evt.getPropertyName();

		if (property.equals(UnifiedGUIModel.PROPERTY_CURRENT_PLAYER)) {
			showHand(guiModel.getCurrentPlayer());

		}
		else if (property.equals(UnifiedGUIModel.PROPERTY_NEW_PLAYER)) {
			addPlayer((Player) evt.getNewValue());

		}
		else if (property.equals(UnifiedGUIModel.PROPERTY_PLAYER_REMOVED)) {
			removePlayer((Player) evt.getOldValue());

		}
		else if (property.equals(UnifiedGUIModel.PROPERTY_PLAYER_DEALT)) {
			handMap.get((Player) evt.getNewValue()).clear();
		}
	}

	private void showHand(Player player) {
		if (player == UnifiedGUIModel.DEALER) {
			layout.show(this, dealerHandString);
		}
		else {
			layout.show(this, player.getPlayerName());
		}
	}
}
