package view.centrepanel;

import javax.swing.JTable;

import view.interfaces.UnifiedGUIModel;

/**
 * A player summary table, using the JTable(TableModel) constructor.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class SummaryTable extends JTable
{
	public SummaryTable(UnifiedGUIModel guiModel) {
		super(guiModel.getPlayerTable());

		this.setEnabled(false);
		this.setFillsViewportHeight(true);
	}
}
