package view.centrepanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import controller.SummaryPanelResizer;
import view.interfaces.UnifiedGUIModel;

/**
 * Contains the SummaryPanel and the GameMatPanel.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class CentrePanel extends JPanel
{
	private static final double SUMMARY_PANEL_PROPORTION = 1.0 / 5;
	private JScrollPane summaryPanel;
	private GameMat gameMat;

	public CentrePanel(Frame frame, UnifiedGUIModel guiModel) {

		SummaryPanelResizer myAdapter = new SummaryPanelResizer(this);
		this.addComponentListener(myAdapter);

		setLayout(new BorderLayout());

		summaryPanel = new JScrollPane(new SummaryTable(guiModel));
		add(summaryPanel, BorderLayout.NORTH);

		gameMat = new GameMat(guiModel);
		add(gameMat, BorderLayout.CENTER);

	}

	/**
	 * Sets the summary panel to take up no more than SUMMARY_PANEL_PROPORTION as a proportion
	 * of the CentrePanel's height.
	 */
	public void resizeComponents() {
		// Forces the correct size if the window is being maximised/un-maximised.
		revalidate();

		Dimension newSummarySize = getSize();
		newSummarySize.height = (int) (newSummarySize.height * SUMMARY_PANEL_PROPORTION);
		summaryPanel.setPreferredSize(newSummarySize);
	}

	/**
	 * Returns the summary Panel's game mat.
	 * 
	 * @return
	 */
	public GameMat getGameMat() { return gameMat; }

}
