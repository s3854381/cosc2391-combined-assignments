package view.centrepanel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.interfaces.PlayingCard;

/**
 * Displays a player's hand in one row of up to 6 cards. Cards are displayed such that there
 * is always room for six cards according to the panel's width.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class HandPanel extends JPanel
{
	private static final int GREEN_FELT = 0x477148;

	private final int maxCards = 6;
	private int cardsOnScreen = 0;
	private List<CardLabel> cardLabels;
	private GridLayout layout;

	// Card gap size calculations
	private final int cardGaps = maxCards + 1;
	private final double gapWidthUnit = 1.0;
	private final double cardWidthUnit = gapWidthUnit * 20;
	private final double totalWidths = maxCards * cardWidthUnit + cardGaps * gapWidthUnit;
	private final double hGapProportion = gapWidthUnit / totalWidths;

	public HandPanel() {
		setBackground(new Color(GREEN_FELT));
		layout = new GridLayout(1, maxCards);
		setLayout(layout);
		cardLabels = new ArrayList<>();

		for (int i = 0; i < maxCards; ++i) {
			CardLabel label = new CardLabel();
			cardLabels.add(label);
			add(label);
		}
	};

	/**
	 * Adds a card to the display.
	 * 
	 * @param card - the card to display.
	 */
	public void addCard(PlayingCard card) {

		CardLabel label = cardLabels.get(cardsOnScreen);

		label.setCard(card);

		repaint();

		++cardsOnScreen;
	}

	private void resizeComponents() {
		int newGap = (int) (getWidth() * hGapProportion);

		setBorder(new EmptyBorder(0, newGap, 0, newGap));
		layout.setHgap(newGap);
	}

	@Override
	public void paintComponent(Graphics g) { resizeComponents(); super.paintComponent(g); }

	/**
	 * Removes all cards from the display.
	 */
	public void clear() {
		for (CardLabel c : cardLabels) {
			c.setCard(null);
			cardsOnScreen = 0;
		}
	}
}
