package view.interfaces;

import model.interfaces.Player;

/**
 * Interface for a table of players.
 * 
 * @author Michael Asquith
 */
public interface PlayerTable
{
	/**
	 * Possible game results, with an overridden toString() method.
	 * 
	 * @author Michael Asquith
	 */
	enum WinLossResult {
		WIN("Win"), LOSS("Loss"), DRAW("Draw"), DID_NOT_PLAY("Did not play");
	
		private String label;
	
		WinLossResult(String label) { this.label = label; }
	
		@Override
		public String toString() { return this.label; }
	
	}

	/**
	 * Add a player to the table.
	 * @param player - the player to add.
	 */
	public void addPlayer(Player player);

	/**
	 * Remove a player from the table.
	 * @param player - the player to remove
	 */
	public void removePlayer(Player player);

	/**
	 * Notify the table to update the player's bet.
	 * @param player - the player whose bet to update
	 */
	public void betIsSet(Player player);

	/**
	 * Notify the table to update the player's result.
	 * @param player - the player whose result to update
	 * @param result - the player's result
	 */
	public void setResult(Player player, int result);

	/**
	 * Perform finishing actions for a player after the game is finished.
	 * @param player - the player to perform actions for
	 * @param winLossResult - the player's win/loss result from the game
	 */
	public void gameIsFinished(Player player, PlayerTable.WinLossResult winLossResult);
	
	/**
	 * Returns true if the selected player has been dealt a hand in this game.
	 * @param player - the player to check if they have been dealt a hand
	 * @return true if player has been dealt a hand
	 */
	public boolean hasDealt(Player player);
	
	/**
	 * Sets whether a player has been dealt a hand in the game.
	 * @param player - the player whose dealt status to set.
	 * @param hasDealt - the status to set
	 */
	public void setHasDealt(Player player, boolean hasDealt);

}
