package view.interfaces;

import java.beans.PropertyChangeListener;

import javax.swing.table.TableModel;

import model.interfaces.GameEngine;
import model.interfaces.Player;

/**
 * View Model for the poker-like game. Extends the GameEngine interface, but should not
 * implement the game functionality itself. The game should be handled by a delegate, with the
 * UnifiedGUIModel calling extra methods relevant to the GameModel where necessary.
 * 
 * @author Michael Asquith
 */
public interface UnifiedGUIModel extends GameEngine
{
	String PROPERTY_CURRENT_PLAYER = "CURRENT_PLAYER";
	String PROPERTY_READY_TO_DEAL = "READY_TO_DEAL";
	String PROPERTY_READY_TO_BET = "READY_TO_BET";
	String PROPERTY_ALL_PLAYERS_DEALT = "ALL_PLAYERS_DEALT";
	String PROPERTY_NEW_PLAYER = "NEW_PLAYER";
	String PROPERTY_PLAYER_REMOVED = "REMOVED_PLAYER";
	String PROPERTY_PLAYER_LOST = "PLAYER_LOST";
	String PROPERTY_READY_TO_CLEAR_BET = "READY_TO_CLEAR_BET";
	String PROPERTY_PLAYER_DEALT = "PLAYER_DEALT";

	/**
	 * This view model uses a player set to null to represent the dealer.
	 */
	Player DEALER = null;

	/**
	 * Sets the View's currently selected player.
	 * 
	 * @param player - the player to select.
	 */
	void setCurrentPlayer(Player player);

	/**
	 * @return the currently selected player.
	 */
	Player getCurrentPlayer();

	/**
	 * @return true if the dealer is currently selected.
	 */
	boolean dealerSelected();

	/**
	 * @return the name of the currently selected player.
	 */
	String getCurrentPlayerName();

	/**
	 * @return the name of the dealer in this model.
	 */
	String getDealerName();

	/**
	 * Checks if the current player is ready to have their bet cleared.
	 * 
	 * @return true if the player is ready.
	 */
	boolean readyToClearBet();

	/**
	 * Checks if the current player is ready to be dealt to.
	 * 
	 * @return true if player is ready.
	 */
	boolean readyToDeal();

	/**
	 * Checks if the current player is ready to bet.
	 * 
	 * @return true if player is ready.
	 */
	boolean readyToBet();

	/**
	 * @return A TableModel with information to be displayed by the gui, ideally in a JTable.
	 */
	TableModel getPlayerTable();

	/**
	 * Performs actions for a player, after the house has been dealt cards and the game is
	 * finished.
	 * 
	 * @param player        - the player to perform finishing tasks for
	 * @param playerWinLoss - the win/loss result of the player
	 */
	void gameIsFinished(Player player, PlayerTable.WinLossResult playerWinLoss);

	/**
	 * Sets a player's bet to 0.
	 * 
	 * @param player - the player whose bet to reset.
	 */
	void resetBet(Player player);

	/**
	 * Perform actions for a player, after they have been dealt their cards.
	 * 
	 * @param player - the player to perform actions for
	 * @param result - the player's hand result
	 */
	void finishDealingPlayer(Player player, int result);

	/**
	 * Perform finishing actions for the game, after the house has been dealt cards and the
	 * game is finished.
	 */
	void finishedDealingHouse();

	void addPropertyChangeListener(PropertyChangeListener l);

	void removePropertyChangeListener(PropertyChangeListener l);

}
