package view.toolbar;

import java.beans.PropertyChangeEvent;

import controller.DealPlayerController;
import view.interfaces.UnifiedGUIModel;
import view.model.UnifiedGUIModelImpl;

/**
 * Button that deals for a player.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class DealButton extends ListeningToolBarButton
{
	public DealButton(UnifiedGUIModelImpl guiModel) {
		super(guiModel, "Deal");

		addActionListener(new DealPlayerController(guiModel));
		setEnabled(false);
	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {

		if (arg0.getPropertyName().equals(UnifiedGUIModel.PROPERTY_READY_TO_DEAL)) {
			setEnabled(guiModel.readyToDeal());
		}
	}
}
