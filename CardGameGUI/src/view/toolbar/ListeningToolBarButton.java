package view.toolbar;

import java.beans.PropertyChangeListener;

import javax.swing.JButton;

import view.interfaces.UnifiedGUIModel;

/**
 * Abstract class for ToolBar Buttons that listen to the UnifiedGUIModel.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public abstract class ListeningToolBarButton extends JButton implements PropertyChangeListener
{
	protected UnifiedGUIModel guiModel;

	protected ListeningToolBarButton(UnifiedGUIModel guiModel, String name) {
		super(name);
		this.guiModel = guiModel;
		guiModel.addPropertyChangeListener(this);
	}
}
