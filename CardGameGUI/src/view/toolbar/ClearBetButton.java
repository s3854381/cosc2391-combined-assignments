package view.toolbar;

import java.beans.PropertyChangeEvent;

import controller.ClearBetController;
import view.interfaces.UnifiedGUIModel;

/**
 * Button that clears a player's bet.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class ClearBetButton extends ListeningToolBarButton
{
	public ClearBetButton(UnifiedGUIModel guiModel) {
		super(guiModel, "Clear Bet");

		addActionListener(new ClearBetController(guiModel));
		setEnabled(false);
	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {

		if (arg0.getPropertyName() == UnifiedGUIModel.PROPERTY_READY_TO_CLEAR_BET) {
			setEnabled(guiModel.readyToDeal());
		}
	}

}
