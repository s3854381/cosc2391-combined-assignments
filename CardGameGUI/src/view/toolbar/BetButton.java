package view.toolbar;

import java.awt.Frame;
import java.beans.PropertyChangeEvent;

import controller.BetButtonController;
import view.interfaces.UnifiedGUIModel;
import view.model.UnifiedGUIModelImpl;

/**
 * Button that brings up the bet dialog.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class BetButton extends ListeningToolBarButton
{
	public BetButton(Frame frame, UnifiedGUIModelImpl guiModel) {
		super(guiModel, "Bet");

		addActionListener(new BetButtonController(frame, guiModel));
		setEnabled(false);
	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {

		if (arg0.getPropertyName().equals(UnifiedGUIModel.PROPERTY_READY_TO_BET)) {
			setEnabled(guiModel.readyToBet());
		} ;
	}
}
