package view.toolbar;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.BetDialogController;
import view.model.UnifiedGUIModelImpl;

/**
 * Dialog box to get the details of a new player to be added to the game. Based off the Java
 * Tutorial's custom dialog box at:
 * https://docs.oracle.com/javase/tutorial/displayCode.html?code=https://docs.oracle.com/javase/tutorial/uiswing/examples/components/DialogDemoProject/src/components/CustomDialog.java
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class BetDialog extends JDialog implements ActionListener
{

	private final String defaultValue = "20";
	private final String frameTitle = "Set Bet";
	private final String question = "How much would you like to bet?";
	private final String btnString1 = "Bet";
	private final String btnString2 = "Cancel";

	private JOptionPane optionPane;
	private JTextField textField;
	private UnifiedGUIModelImpl guiModel;

	public BetDialog(Frame frame, UnifiedGUIModelImpl guiModel) {
		super(frame, true);
		this.guiModel = guiModel;
		setTitle(frameTitle);
		setSize(300, 200);

		int frameX = frame.getX();
		int frameY = frame.getY();
		int frameWidth = frame.getWidth();
		int frameHeight = frame.getHeight();
		int thisX = frameX + frameWidth / 2 - getWidth() / 2;
		int thisY = frameY + frameHeight / 2 - getHeight() / 2;
		this.setLocation(thisX, thisY);

		textField = new JTextField(defaultValue);
		Object[] array = { question, textField };

		Object[] options = { btnString1, btnString2 };

		optionPane = new JOptionPane(array, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION,
				null, options, options[0]);

		setContentPane(optionPane);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		// Ensure the text field always gets the first focus.
		addComponentListener(new ComponentAdapter()
		{
			public void componentShown(ComponentEvent ce) { textField.requestFocusInWindow(); }
		});

		// Register an event handler that puts the text into the option pane.
		textField.addActionListener(this);

		// Register an event handler that reacts to option pane state changes.
		optionPane.addPropertyChangeListener(new BetDialogController(guiModel, this));

		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) { optionPane.setValue(btnString1); }

	public JOptionPane getOptionPane() { return optionPane; }

	public String getBtnString1() { return btnString1; }

	public String getBet() { return textField.getText(); }

	public void displayerErrorMessage(boolean isInt, boolean correctRange) {

		String errorMessage;
		int maxBet = guiModel.getCurrentPlayer().getPoints();
		if (!isInt) {
			errorMessage = String.format("Please enter a whole number between 1 and %d.", maxBet);
		}
		else if (!correctRange) {
			errorMessage = String.format("Please enter a number between 1 and %d.", maxBet);
		}
		else {
			errorMessage = "Something strange happened. Try again.";
		}

		textField.selectAll();
		JOptionPane.showMessageDialog(this, errorMessage, "Try again", JOptionPane.ERROR_MESSAGE);
		textField.requestFocusInWindow();
	}

}
