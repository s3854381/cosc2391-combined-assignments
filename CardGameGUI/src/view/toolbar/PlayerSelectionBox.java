package view.toolbar;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;

import controller.PlayerSelectionController;
import model.interfaces.Player;
import view.interfaces.UnifiedGUIModel;

/**
 * Drop down box that allows the user to select a player whose hand to display.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class PlayerSelectionBox extends JComboBox<String> implements PropertyChangeListener
{
	private List<Player> players = new ArrayList<>();

	public PlayerSelectionBox(UnifiedGUIModel guiModel) {
		guiModel.addPropertyChangeListener(this);
		addActionListener(new PlayerSelectionController(guiModel, this));
		players.add(guiModel.getCurrentPlayer());
		addItem(guiModel.getCurrentPlayerName());
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {

		if (evt.getPropertyName().equals(UnifiedGUIModel.PROPERTY_NEW_PLAYER)) {
			Player newPlayer = (Player) evt.getNewValue();

			// New player may be equivalent to a previous player.
			if (players.contains(newPlayer)) { removePlayer(newPlayer); }

			players.add(newPlayer);
			addItem(newPlayer.getPlayerName());
		}
		else if (evt.getPropertyName().equals(UnifiedGUIModel.PROPERTY_PLAYER_REMOVED)) {
			removePlayer((Player) evt.getOldValue());
		}
		else if (evt.getPropertyName().equals(UnifiedGUIModel.PROPERTY_CURRENT_PLAYER)) {
			int index = players.indexOf(evt.getNewValue());
			setSelectedIndex(index);
		}
	}

	private void removePlayer(Player player) {
		players.remove(player);
		removeItem(player.getPlayerName());
	}

	/**
	 * @return the player currently selected in this box.
	 */
	public Player getCurrentPlayer() { return players.get(getSelectedIndex()); }
}
