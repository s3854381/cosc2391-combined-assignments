package view.toolbar;

import java.awt.Dimension;
import java.awt.Frame;

import javax.swing.JToolBar;

import view.model.UnifiedGUIModelImpl;

/**
 * The game's top toolbar. Has buttons to clear a bet, deal for a player and bet for a player,
 * as well as the player selection box.
 * 
 * @author Michael Asquith
 */
@SuppressWarnings("serial")
public class GameToolBar extends JToolBar
{
	// Summary Panel
	public GameToolBar(Frame frame, UnifiedGUIModelImpl guiModel) {
		setPreferredSize(new Dimension(300, 30));
		add(new ClearBetButton(guiModel));
		add(new DealButton(guiModel));
		add(new BetButton(frame, guiModel));
		add(new PlayerSelectionBox(guiModel));
		setFloatable(false);
	}

}
