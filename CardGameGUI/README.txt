Instructions:

ADDING PLAYERS:

The game starts with no players. To add a player go to the Player menu and choose "Add player...". A dialog
menu will appear allowing you to input their name and starting points. They cannot have the same name as
a player currently in the game, including "The House".

SELECTING PLAYERS:

Use the player selection drop-down box to select which player to display.

REMOVING PLAYERS:

Select the player you would like to remove from the player selection drop-down box. Then go to the Player menu
and choose "Remove current player...". Select "Yes" in the confirmation dialog.

Design Note: I think this could be improved with a separate dialog. The dialog would have a ComboBox
containing the current list of players, which could be removed from there. This means you do not have to
switch to that player before removing them. This was not possible in an earlier in the design when I was not
enforcing unique player names.

BETTING:

Choose the player you wish to bet for from the player selection drop-down, then click "Bet". A dialog box will
appear allowing you to select how much you would like to bet. Betting is not possible if the player has
already been dealt cards.

CLEARING A BET:

Choose the player whose bet you wish to clear, then click "Clear Bet". Clearing a bet is not possible if the
player has no bet, or has already been dealt cards.

DEALING: 

Choose the player you want to deal for, then click "Deal". Dealing is not possible if the player has no bet,
has already been dealt cards, or another player is currently being dealt cards.

DEALING THE HOUSE:

The house automatically deals if either:
a) The last player who has not yet been dealt for is dealt for.
b) The last player who has not yet been dealt for is removed, and there is still at least one other player.

OTHER DESIGN NOTES:

The two custom Dialogs, AddPlayerDialog and BetDialog, could be abstracted into an AbstractDialog to reduce
code duplication.