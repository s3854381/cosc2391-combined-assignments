CardGame and CardGameGUI were assignments 1 and 2 respectively for COSC2391 Further Programming.

CardGame required the implementation of the game model through provided interfaces, and a simple text output describing the events of the game.

CardGameGUI uses the model from CardGame, the MVC structure, and Java Swing to create a Graphical User Interface of the game produced from Assignment 1.

Source control history is not available for this repository.